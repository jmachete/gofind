package com.android.gofind.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.gofind.GlobalApp;
import com.android.gofind.R;
import com.android.gofind.models.NewsModel;
import com.android.gofind.utils.StringUtils;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

/**
 * Created by jmachete on 03/11/14.
 */
public class DetailsFragment extends Fragment {

    private final static String TAG = DetailsFragment.class.getSimpleName();

    private static final String ARG_IMAGE = "param_image";
    private static final String ARG_TITLE = "param_title";
    private static final String ARG_CONTENT = "param_content";

    private String mParamImageUrl;
    private String mParamTitle;
    private String mParamContent;
    private ImageLoader imageLoader;
    private TextView mTextViewTitle;

    /**
     * * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param model news model
     * @return New instance of the fragment DetailsFragment
     */
    public static DetailsFragment newInstance(NewsModel model) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_IMAGE, model.url);
        args.putString(ARG_TITLE, model.title);
        args.putString(ARG_CONTENT, model.content);
        fragment.setArguments(args);
        return fragment;
    }

    public DetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Tell the framework to try to keep this fragment around
        // during a configuration change.
        setRetainInstance(true);

        if (getArguments() != null) {
            mParamImageUrl = getArguments().getString(ARG_IMAGE);
            mParamTitle = getArguments().getString(ARG_TITLE);
            mParamContent = getArguments().getString(ARG_CONTENT);
        }

        imageLoader = GlobalApp.getInstance().getImageLoader();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((NetworkImageView) view.findViewById(R.id.image))
                .setImageUrl(mParamImageUrl, imageLoader);

        if (!StringUtils.isEmpty(mParamContent) && !StringUtils.isEmpty(mParamTitle)) {
            mTextViewTitle = (TextView) view.findViewById(R.id.news_title);
            mTextViewTitle.setText(Html.fromHtml(String.valueOf(mParamTitle)).toString());
            mTextViewTitle.setTag(R.id.news_title);
            ((TextView) view.findViewById(R.id.news_content)).setText(
                    Html.fromHtml(String.valueOf(mParamContent)).toString());
        }
    }
}
