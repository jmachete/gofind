package com.android.gofind;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.android.gofind.models.NewsModel;
import com.android.gofind.utils.Constants;
import com.android.gofind.utils.LruBitmapCache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmachete on 03/11/14.
 */
public class GlobalApp extends Application {

    /**
     * A singleton instance of the application class for easy access in other places
     */
    private static GlobalApp mInstance;

    /**
     * Volley request queue
     */
    private RequestQueue mRequestQueue;

    /**
     * Volley image loader
     */
    private ImageLoader mImageLoader;

    /**
     * Global News Model
     */
    public List<NewsModel> mNewsModel;

    @Override
    public void onCreate() {
        super.onCreate();

        // initialize the singleton
        mInstance = this;
        mNewsModel = new ArrayList<NewsModel>();
        mRequestQueue = Volley.newRequestQueue(getApplicationContext());
    }

    /**
     * @return ApplicationController singleton instance
     */
    public static synchronized GlobalApp getInstance() {
        return mInstance;
    }

    public ImageLoader getImageLoader() {
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(newRequestQueue(getApplicationContext()),
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    /**
     * Use external cache directory to cache images.
     * Most code copied from "Volley.newRequestQueue(..)", changed only cache directory
     */
    private static RequestQueue newRequestQueue(Context context) {
        // define cache folder
        File rootCache = context.getExternalCacheDir();
        if (rootCache == null) {
            //Can't find External Cache Dir, switching to application specific cache directory
            rootCache = context.getCacheDir();
        }

        File cacheDir = new File(rootCache, Constants.DEFAULT_CACHE_DIR);
        cacheDir.mkdirs();

        HttpStack stack = new HurlStack();
        Network network = new BasicNetwork(stack);
        DiskBasedCache diskBasedCache = new DiskBasedCache(cacheDir, Constants.DEFAULT_DISK_USAGE_BYTES);
        RequestQueue queue = new RequestQueue(diskBasedCache, network);
        queue.start();

        return queue;
    }

    /**
     * Adds the specified request to the global queue, if tag is specified then
     * it is used else Default TAG is used.
     *
     * @param req
     * @param tag
     */
    public <T> void addToRequestQueue(final Request<T> req,
                                      final String tag) {

        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? Constants.DEFAULT_REQUEST_TAG
                : tag);

        // Timeout and Retry
        req.setRetryPolicy(
                new DefaultRetryPolicy(
                        Constants.DEFAULT_TIMEOUT_MS,
                        Constants.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

        mRequestQueue.add(req);
    }

    /**
     * Adds the specified request to the global queue using the Default TAG.
     *
     * @param req
     */
    public <T> void addToRequestQueue(final Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(Constants.DEFAULT_REQUEST_TAG);

        // Timeout and Retry
        req.setRetryPolicy(
                new DefaultRetryPolicy(
                        Constants.DEFAULT_TIMEOUT_MS,
                        Constants.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

        mRequestQueue.add(req);
    }

    /**
     * Cancels all pending requests by the specified TAG, it is important to
     * specify a TAG so that the pending/ongoing requests can be cancelled.
     *
     * @param tag
     */
    public void cancelPendingRequests(final String tag) {
        mRequestQueue.cancelAll(TextUtils.isEmpty(tag) ? Constants.DEFAULT_REQUEST_TAG
                : tag);
    }
}
