package com.android.gofind.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.gofind.Fragments.DetailsFragment;
import com.android.gofind.GlobalApp;
import com.android.gofind.R;
import com.android.gofind.adapters.NewsFragmentAdapter;

public class DetailsActivity extends ActionBarActivity {

    private final static String TAG = DetailsActivity.class.getSimpleName();
    private MenuItem toggleItem;
    private boolean isToggle = false;
    private ViewPager mPager;
    private int currentPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        //starts the fragment transaction
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new DetailsFragment(), DetailsFragment.class.getName())
                    .commit();
        }

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new NewsFragmentAdapter(
                getSupportFragmentManager(), GlobalApp.getInstance().mNewsModel.size()));

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                currentPosition = i;
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        //gets the selected position from the list
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int position = extras.getInt(MainActivity.EXTRA_POSITION);
            mPager.setCurrentItem(position);
        }

        initActionBar();
    }

    private void initActionBar() {
        // Set up the action bar.
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setIcon(R.drawable.transparent);
        actionBar.setTitle(getString(R.string.title_activity_details));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        toggleItem = menu.findItem(R.id.action_change_color);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_change_color:
                toggleButton();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * Toggle the actionbar button ON and OFF
     */
    public void toggleButton() {
        isToggle = !isToggle;

        //lets set the avatar with the picture the user selected
        DetailsFragment fragment = (DetailsFragment) getSupportFragmentManager()
                .findFragmentByTag(DetailsFragment.class.getName());

        TextView title = (TextView) mPager.findViewById(R.id.news_title);

        if (isToggle) {
            toggleItem.setIcon(getResources().getDrawable(R.drawable.image_on));
            title.setTextColor(getResources().getColor(R.color.solid_green));
        } else {
            toggleItem.setIcon(getResources().getDrawable(R.drawable.image_off));
            title.setTextColor(getResources().getColor(R.color.solid_white));
        }

    }

}
