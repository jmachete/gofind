package com.android.gofind.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.gofind.GlobalApp;
import com.android.gofind.R;
import com.android.gofind.adapters.NewsListAdapter;
import com.android.gofind.models.NewsModel;
import com.android.gofind.models.NewsModelParser;
import com.android.gofind.utils.Constants;
import com.android.gofind.utils.Log;
import com.android.gofind.ws.NewsRequestManager;
import com.google.gson.Gson;

public class MainActivity extends ActionBarActivity implements NewsRequestManager.GetJSONListener {

    private final static String TAG = MainActivity.class.getSimpleName();
    public final static String EXTRA_POSITION = TAG + "_POSITION";

    private NewsListAdapter mAdapter;
    private View mProgressLabel, mProgressView, mTextViewNoResults;
    private InputMethodManager inputMethodManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView mListView = (ListView) findViewById(R.id.listview);
        mAdapter = new NewsListAdapter(this, R.layout.list_item, GlobalApp.getInstance().mNewsModel);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                startDetailsActivity(position);
            }

        });

        mProgressView = findViewById(R.id.progress);
        mProgressLabel = findViewById(R.id.progress_label);
        mTextViewNoResults = findViewById(R.id.no_results);

        inputMethodManager = (InputMethodManager) this
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        initActionBar();

    }

    private void initActionBar() {
        // Set up the action bar.
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setIcon(R.drawable.transparent);
        actionBar.setTitle(getString(R.string.title_activity_main));
    }

    /**
     * Called when the user clicks the list item
     */
    private void startDetailsActivity(int position) {
        Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
        intent.putExtra(EXTRA_POSITION, position);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        mSearchView.requestFocus();

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                //query the google API
                NewsRequestManager.getInstance(MainActivity.this).getNews(
                        query.toLowerCase(), Constants.DEFAULT_REQUEST_TAG);

                showProgress(true);

                //hide the keyboard after submit
                try {
                    inputMethodManager.hideSoftInputFromWindow(
                            MainActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                } catch (NullPointerException e) {
                    Log.d(TAG, e.getMessage(), e);
                }

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                mTextViewNoResults.setVisibility(View.VISIBLE);

                //cancel any previous request
                GlobalApp.getInstance().cancelPendingRequests(Constants.DEFAULT_REQUEST_TAG);
                //clean any previous data
                GlobalApp.getInstance().mNewsModel.clear();
                mAdapter.clear();

                return false;
            }
        });

        return super.onPrepareOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_search:
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onGetNewsRequestComplete(String jsonFromRequest) {

        //clear old data
        GlobalApp.getInstance().mNewsModel.clear();

        //parse the response
        NewsModelParser modelParser = new Gson().fromJson(jsonFromRequest,
                NewsModelParser.class);

        Log.e(TAG, "" + modelParser.toString());

        //populate the array with NewsModel data
        for (NewsModelParser.Results result : modelParser.responseData.results) {

            GlobalApp.getInstance().mNewsModel.add(new NewsModel(
                    result.image != null ? result.image.url : "",
                    result.title != null ? result.title : "",
                    result.content != null ? result.content : "",
                    result.publishedDate != null ? result.publishedDate : ""
            ));
        }

        //notify observers that data changed
        mAdapter.notifyDataSetChanged();

        showProgress(false);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {
        mTextViewNoResults.setVisibility(View.GONE);
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressLabel.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onGetRequestError(Exception error) {
        Toast.makeText(getApplicationContext(), getString(R.string.request_error), Toast.LENGTH_SHORT).show();
        showProgress(false);
    }
}
