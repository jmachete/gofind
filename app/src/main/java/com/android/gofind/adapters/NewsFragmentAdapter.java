package com.android.gofind.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.gofind.Fragments.DetailsFragment;
import com.android.gofind.GlobalApp;

/**
 * Created by jmachete on 03/11/14.
 */
public class NewsFragmentAdapter extends FragmentPagerAdapter {

    private int size;

    public NewsFragmentAdapter(FragmentManager fm, int size) {
        super(fm);
        this.size = size;
    }

    @Override
    public Fragment getItem(int position) {
        return DetailsFragment.newInstance(
                GlobalApp.getInstance().mNewsModel.get(position));
    }

    @Override
    public int getCount() {
        return size;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
