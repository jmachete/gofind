package com.android.gofind.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.gofind.GlobalApp;
import com.android.gofind.R;
import com.android.gofind.models.NewsModel;
import com.android.gofind.views.NetworkImageViewRounded;
import com.android.volley.toolbox.ImageLoader;

import java.util.List;

public class NewsListAdapter extends ArrayAdapter<NewsModel> {

    private List<NewsModel> newsItems;
    private ImageLoader imageLoader;
    private int resource;

    public NewsListAdapter(final Context context, int resource, final List<NewsModel> newsItems) {
        super(context, resource, newsItems);
        this.resource = resource;
        this.newsItems = newsItems;
    }

    /**
     * The Class ViewHolder.
     */
    static class ViewHolder {

        private NetworkImageViewRounded thumbNail;
        private TextView title;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {

            final LayoutInflater layoutInflator = LayoutInflater
                    .from(getContext());

            convertView = layoutInflator.inflate(this.resource, null);

            holder = new ViewHolder();

            imageLoader = GlobalApp.getInstance().getImageLoader();
            holder.thumbNail = (NetworkImageViewRounded) convertView
                    .findViewById(R.id.thumbnail);
            holder.title = (TextView) convertView
                    .findViewById(R.id.news_title);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        // getting movie data for the row
        NewsModel m = newsItems.get(position);

        // thumbnail image
        holder.thumbNail.setImageUrl(m.url, imageLoader);
        // set the title
        holder.title.setText(Html.fromHtml((String) m.title).toString());

        return convertView;
    }

}
