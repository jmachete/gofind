package com.android.gofind.models;

/**
 * Created by jmachete on 03/11/14.
 */
public class NewsModel {

    public String url;
    public String title;
    public String content;
    public String date;

    public NewsModel(String url, String title, String content, String date) {
        this.url = url;
        this.title = title;
        this.content = content;
        this.date = date;
    }
}
