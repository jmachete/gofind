package com.android.gofind.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by jmachete on 03/11/14.
 */
public class NewsModelParser {

    @SerializedName("responseData")
    public ResponseData responseData;

    public class ResponseData {
        @SerializedName("results")
        public ArrayList<Results> results = new ArrayList<Results>();
    }

    public class Results {
        @SerializedName("publishedDate")
        public String publishedDate;
        @SerializedName("title")
        public String title;
        @SerializedName("content")
        public String content;
        @SerializedName("image")
        public Image image;
    }

    public class Image {
        @SerializedName("url")
        public String url;
    }

    @Override
    public String toString() {
        return "NewsModelParser{" +
                "responseData=" + responseData +
                '}';
    }
}
