package com.android.gofind.utils;

/**
 * Created by jmachete on 03/11/14.
 */
public class Constants {

    /**
     * API
     */
    public static final String API_URL = "https://ajax.googleapis.com/ajax/services/search/news?v=1.0&q=";
    public static final String ENDPOINT_LANGUAGE_OPTION = "&language=en";

    /**
     * Volley Request
     */
    public static final int DEFAULT_TIMEOUT_MS = 30000;
    public static final int DEFAULT_MAX_RETRIES = 2;

    /**
     * Volley maximum disk usage in bytes
     */
    public static final int DEFAULT_DISK_USAGE_BYTES = 25 * 1024 * 1024;

    /**
     * Volley cache folder name
     */
    public static final String DEFAULT_CACHE_DIR = "photos";

    /**
     * Volley Level of importance request TAG
     */
    public static final String HIGH_REQUEST_TAG = "HighRequestGroup";
    public static final String DEFAULT_REQUEST_TAG = "DefaultRequestGroup";
}
