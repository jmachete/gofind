package com.android.gofind.utils;

public class StringUtils {


    /**
     * First letter caps.
     *
     * @param data the data
     * @return the string
     */
    public static String firstLetterCaps(final String data) {
        final String firstLetter = data.substring(0, 1).toUpperCase();
        final String restLetters = data.substring(1).toLowerCase();
        return firstLetter + restLetters;
    }

    /**
     * Checks if is empty.
     *
     * @param str the str
     * @return true, if is empty
     */
    public static boolean isEmpty(final String str) {
        return str == null || str.length() == 0;
    }

}
