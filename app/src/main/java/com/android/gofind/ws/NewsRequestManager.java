package com.android.gofind.ws;

import com.android.gofind.GlobalApp;
import com.android.gofind.utils.Constants;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class NewsRequestManager {

    private static final String TAG = NewsRequestManager.class.getSimpleName();

    /**
     * The listener interface for receiving getJSON events. The class that is
     * interested in processing a getJSON event implements this interface, and
     * the object created with that class is registered with a component using
     * the component's <code>addGetJSONListener<code> method. When
     * the getJSON event occurs, that object's appropriate
     * method is invoked.
     */
    public interface GetJSONListener {

        /**
         * On Get News remote call complete.
         *
         * @param jsonFromRequest the json from request
         */
        void onGetNewsRequestComplete(String jsonFromRequest);

        /**
         * On remote call error
         *
         * @param error
         */
        void onGetRequestError(Exception error);

    }

    private static NewsRequestManager instance;

    /**
     * The get json listener.
     */
    private final GetJSONListener getJSONListener;

    // This method should be called first to do singleton initialization
    public static synchronized NewsRequestManager getInstance(final GetJSONListener listener) {
        if (instance == null) {
            instance = new NewsRequestManager(listener);
        }
        return instance;
    }

    public static synchronized NewsRequestManager getInstance() {
        if (instance == null) {
            throw new IllegalStateException(TAG +
                    " is not initialized, call getInstance(..) method first.");
        }
        return instance;
    }

    /**
     * Instantiates a new access request.
     *
     * @param listener the listener
     */
    public NewsRequestManager(final GetJSONListener listener) {
        this.getJSONListener = listener;
    }

    /**
     * Gets the news.
     */
    public void getNews(final String query, final String tag) {

        String url = "";
        try {
            url = Constants.API_URL + URLEncoder.encode(query, "utf-8") + Constants.ENDPOINT_LANGUAGE_OPTION;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // prepare the Request
        final JsonObjectRequest getRequest = new JsonObjectRequest(
                Request.Method.GET, url, null,
                createSuccessListener(),
                createErrorListener());

        GlobalApp.getInstance().addToRequestQueue(getRequest, tag);
    }

    /**
     * Creates the success listener.
     *
     * @return the response. listener
     */
    private Response.Listener<JSONObject> createSuccessListener() {
        return new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(final JSONObject response) {
                //Returns the current json to whoever's waiting for it
                returnOk(response);
            }
        };
    }

    /**
     * Creates the error listener.
     *
     * @return the response. error listener
     */
    private Response.ErrorListener createErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                returnError(error);
            }
        };
    }

    /**
     * Return ok.
     *
     * @param response the response
     */
    private void returnOk(final JSONObject response) {
        NewsRequestManager.this.getJSONListener
                .onGetNewsRequestComplete(response.toString());
    }

    /**
     * Return error.
     *
     * @param error Error ocurred in Volley
     */
    private void returnError(Exception error) {
        NewsRequestManager.this.getJSONListener
                .onGetRequestError(error);

    }
}
